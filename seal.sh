#! /bin/bash

ACCEPT='[yY]'

if [[ $# -gt 0 ]]; then
  FILES=$*
  REPLACE='y'
else
  FILES=$(find . -name '*.template.yaml')
  read -r -p 'Do you want replace all already sealed secrets? [y/N] ' REPLACE
fi;

for FILE in $FILES; do
  TARGET=${FILE/\.template/}

  if [[ -f $TARGET && ! $REPLACE =~ $ACCEPT ]]; then
    echo "Skipping $FILE since $TARGET already exists."
  else
    echo -n "Sealing $FILE to $TARGET... "

    if kubeseal --format=yaml < "$FILE" > "$TARGET"; then
      echo "ok."
    else
      echo "failed."
    fi;
  fi;
done;
