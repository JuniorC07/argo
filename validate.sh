#! /bin/bash

CONTEXT=${1:-$CONTEXT}
CONTEXT=${CONTEXT:-"homologation"}

YAMLS=$(find . -iname '*.yaml' | grep -v '.template.')
FAILED=""

for YAML in $YAMLS; do
  printf "\nValidating %s...\n" "$YAML"

  if ! kubectl --context "$CONTEXT" apply --dry-run --validate -f "$YAML"; then
    FAILED="$FAILED $YAML"
  fi;
done;

if [ ${#FAILED} -gt 0 ]; then
  printf "\nWhoops!\n"
  echo "Check out these YAMLs:"

  for YAML in $FAILED; do
    echo "$YAML"
  done;

  exit 1
fi;
